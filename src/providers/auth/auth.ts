import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TokenStorageProvider } from '../token-storage/token-storage';
import { tap, map, switchMap, catchError } from 'rxjs/operators';
import { ILogin } from "../../model/models";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';

import { ToastController, App } from 'ionic-angular';

// import { AccessData, ILogin } from '../../models/models';
import {LoginPage} from "../../pages/login/login";


export interface AccessData {
  access_token: string;
  refresh_token: string;
}

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  // apiRoot: string = "http://192.168.0.123:8000/api";
  apiRoot: string = "http://192.168.1.13:8000/api";
  valauten:boolean = false;
  constructor(public http: HttpClient,
              private app:App,
              public toastCtrl: ToastController,
              private tokenStorage: TokenStorageProvider) {
    console.log('Hello AuthProvider Provider');
    this.getAccessTokenn();
  }
  headerDefault(): any {
    let bearerToken;
    this.getAccessTokenn().subscribe((data) =>{
      bearerToken =  data;
    });
    const httpOptions = {
      headers: new HttpHeaders({
        // 'Accept': 'application/json',
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${bearerToken}`
      })
    };
    return httpOptions;
  }

  isRreshToken(): Observable<boolean> {
    return this.tokenStorage
      .getRefreshToken()
      .map(token => !!token);
  }
  isAccessToken(): Observable<boolean> {
    return this.tokenStorage
      .getAccessToken()
      .map(token => !!token);
  }

  getAccessTokenn(): Observable <string> {
    this.isAccessToken().subscribe((da)=>{
       if(!da) {
            this.tokenStorage
            .setAccessToken('eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjkxMTFlMWQ5N2I3Yjc1ZWI1ZDcyODExNmRmN2MwOWYyOWI0NTdkNzcyMmFmZjUwNmE2ZWZiYmMyZTI1NmI0MWVjN2RhYWE4OTAxMzAyYzZlIn0.eyJhdWQiOiIzIiwianRpIjoiOTExMWUxZDk3YjdiNzVlYjVkNzI4MTE2ZGY3YzA5ZjI5YjQ1N2Q3NzIyYWZmNTA2YTZlZmJiYzJlMjU2YjQxZWM3ZGFhYTg5MDEzMDJjNmUiLCJpYXQiOjE1NDYxMzU1OTQsIm5iZiI6MTU0NjEzNTU5NCwiZXhwIjoxNTc3NjcxNTk0LCJzdWIiOiIxIiwic2NvcGVzIjpbImFwaXdlYiJdfQ.Zx1GFaD9LjQVHYHapsPX5137I18sxXiJO9L4qH_dtIYYM16FIWVl_ITv4VuuQmxN2IBS6H58d-jbV1kQkJt_DW6xIOslo9MBXVDhvmcRU64QMCSefX90t9wpNDraeSpNRwSGJo502mngGpZ0pFZ94mx86E-OgwjhHP2Xh1DORkwc_JtOLKD3HvEXzQ3_ISY8jQ7kuZVpyEt1cZbi0-MHmjpk-np5YDTPgFNTSJWQtlqtcA_3Jq37i493SkJdm70YHmfa4XElTrmJXD6r1e6wSQ1gZ9YfNOOzjJOB_LW6p_UZ5K_ha0cpxdloL8s-kv6IhgkD1DKrbJCSSyN9dZXvEeKwTh2sKb04x1d1iL6ADBz1bmXAE7mBmRA66-PHh8MQ0ITtaR9iD2Jb8SZkEp15g3FtoqbnGygx2xf1-ZznX8EqdgHWlNy0VJsTkXq5dazakgMZiiqqu-DD1UW9AtTGAMiUuJD6LgNnStMoVMhn9npctLLr1YA3WqeB7OUM5_t6k6b1wxpjRNWcxk6xw519tpV3q14vVZtOBFM01_lafbVS_iKUWpqlJOqAjLeMN6TNhDBKsyHoMzLrL2fyRSwmawtm7L1ASuylGcY4X9_UaksTi-VNUetGCdER8dVJIZvNd8gnKkBYLPeiFw32X9cGblMrIC-KDUp8sNDtzXnIQkc');
        }
    });
    return this.tokenStorage.getAccessToken();
  }

  refreshToken(): Observable<AccessData>{
    const url = `${this.apiRoot}/refresh`;
     return this.tokenStorage
    .getRefreshToken()
    .pipe(
      switchMap((refreshToken: string) =>
        // this.http.post(`http://127.0.0.1:8000/api/refresh`, { refreshToken })
        this.http.post(url, {refresh_token: refreshToken,rol:this.tokenStorage.getPayload().scopes[0]}, this.headerDefault())
      ),
      tap((tokens: any) => this.saveAccessData(tokens)),
      catchError((err) => {
        this.logout();
        return Observable.throw(err);
      })
    );
  }
  salir(){
    this.tokenStorage.clear();
    this.getAccessTokenn();
    this.app.getRootNav().setRoot(LoginPage);
  }

  login(data:ILogin): Observable<any>{
    const url = `${this.apiRoot}/login`;
    const da = { 
      username: data.username,
      password: data.password
      // provider: 'adm'
    };
    return this.http.post(url, da, this.headerDefault())
    .pipe(tap((tokens: any) => this.saveAccessData(tokens)))
    .do(() => {
      // this.app.getRootNav().setRoot(LoginPage);
      // this.router.navigate(['/']);
    });
  }

  logout(): Observable<any>{
    console.log('logout');
    
    const url = `${this.apiRoot}/logout`;
    return this.http.post(url, {}, this.headerDefault())
    .pipe(
      tap((tokens: any) => {
      this.salir();
      }),
      catchError((err) => {
        this.salir();
        return Observable.throw(err);
      })
    );
    // .map(response => response.json())
    // .do(() => {
    //   console.log("salio");
      
    //   this.salir();
    // })
    // .catch((err) => {
    //   console.log("error");
      
    //   this.salir();
    //   return Observable.throw(err);
    // });
  }

  saveAccessData({ access_token, refresh_token }: AccessData) {
    this.tokenStorage
      .setAccessToken(access_token)
      .setRefreshToken(refresh_token);
  }

  isAuthenticated(user):boolean{
      let data = false;
        this.isAccessToken().subscribe((da)=>{
            if(da) {
              for (var i = 0; i < user.length; ++i) {
                if(this.tokenStorage.getPayload().scopes[0]==user[i]) {
                  data = true;
                }
              }
            }else{
              this.salir();
            }
        });
    return data;
  }

  errorstatus(content,error){
    let mensaje="";
    for (let a in content) {
      mensaje += content[a][0];
    }
    const toast = this.toastCtrl.create({
      message: mensaje,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
    // console.log(mensaje);
    return mensaje;
  }

}
