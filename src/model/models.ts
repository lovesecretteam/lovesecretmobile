export interface AccessData {
    access_token: string;
    refresh_token: string;
}
export interface ILoginData {
    provider: string;
    provider_user_id: string;
    nombre:string;
    email:string;
}
export interface ILogin {
    username: string;
    password: string;
}
export interface IUser {
    id: number;
    nombre: string;
    empresa?: string;
    ci?: string;
    telefono?: string;
    celular: string;
    usuario: string;
    email?: string;
    password?: string;
    c_password?: string;
    tipo: string;
}

export interface IUsuario {
    id: number;
    alias: string;
    avatar_id: number;
    username: string;
    password: string;
}
  
  