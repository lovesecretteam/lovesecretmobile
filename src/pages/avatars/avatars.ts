import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Restangular } from "ngx-restangular";
import { RegisterPage } from '../register/register';

/**
 * Generated class for the AvatarsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-avatars',
  templateUrl: 'avatars.html',
})
export class AvatarsPage {
  dataavatars:any;
  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams,public restangular: Restangular) {
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.avatars().then( () => {
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AvatarsPage');
  }

  avatars(){
    return new Promise(resolve => {
      this.restangular.all('avatarsdata').customGET('',{}).toPromise()
        .then((data)=> {
          console.log(data.data);
          
          this.dataavatars = data.data;
          // console.log(this.dataavatars);
          
          resolve(true);
        },
        ()=>{
          resolve(false);
        });
    });
  }

  ver(item){
    this.navCtrl.push(RegisterPage, {
      data: item
    });
  }

}
