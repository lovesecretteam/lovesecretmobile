import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListPublicityPage } from './list-publicity';

@NgModule({
  declarations: [
    ListPublicityPage,
  ],
  imports: [
    IonicPageModule.forChild(ListPublicityPage),
  ],
})
export class ListPublicityPageModule {}
