import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController,MenuController, AlertController } from 'ionic-angular';
import { Restangular } from 'ngx-restangular';

import { PublicityPage } from '../publicity/publicity';

/**
 * Generated class for the ListPublicityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-publicity',
  templateUrl: 'list-publicity.html',
})
export class ListPublicityPage {
  img: any = "assets/imgs/log.png"
  noRecords:boolean = false;
  datos:any = [];
  paginador:any={
    actual_page:0,
    ultimo_page:0,
    total:0
  };
  constructor(public alertCtrl: AlertController,private menu: MenuController,public modalCtrl : ModalController, private restangular:Restangular,public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {

    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.cargar('').then( () => {
      console.log(this.datos);

      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListPublicityPage');
  }

  cargar(busca){
    // infiniteScroll.enable(true);
    this.noRecords = false;
    return new Promise(resolve => {
      // let dep = JSON.parse(localStorage.getItem('departamento'));
      let query = {
        order: '1',//ASC DESC RAND
        columna: 'id',
        limit: 10,
        filter:busca,
        page: this.paginador.actual_page+1,
      };
      
      if(query.page==1) {
        this.datos = [];
      }
      this.restangular.all('datapublicidads').getList(query).toPromise()
      .then((data)=> {
          console.log(data);
          let pagination = data[0];
          data.shift();
          if(data.length > 0) {
            this.paginador.actual_page = pagination.current_page;
            this.paginador.ultimo_page = pagination.last_page;
            this.paginador.total = pagination.total;
            for(let cli of data) {
              this.datos.push(cli);
            }
          }
          resolve(true);
      });
    });
  }
  buscar(){
    const prompt = this.alertCtrl.create({
      title: '¿Que buscas?',
      message: "¿Que mensaje publicitario busca?",
      inputs: [
        {
          name: 'busca',
          placeholder: 'Busco'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log(data);
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buscar',
          handler: data => {
            console.log(data.busca);
            let loading = this.loadingCtrl.create({
              // spinner: 'circles',
              content: 'Recuperando datos.'
            });
            loading.present();
            this.paginador.actual_page = 0;
            this.cargar(data.busca).then( () => {
              console.log(this.datos);
        
              loading.dismiss();
            });
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }
  presentProfileModal(item) {
    console.log(item);
    
    let profileModal = this.modalCtrl.create(PublicityPage, { data: item });
    profileModal.present();
  }
  loadData(event) {
    console.log(event);
    this.cargar("").then( () => {
      event.complete();
      if(this.paginador.actual_page === this.paginador.ultimo_page){
        console.log("completa la carga");
        // event.enable(false);
        this.noRecords = true;
      }
    });

  }

}
