import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController,MenuController } from 'ionic-angular';
import { DetailFavoritePage } from '../../pages/detail-favorite/detail-favorite';
import { PublicityPage } from '../publicity/publicity';
import { Restangular } from "ngx-restangular";
import { AppointmentSelectionPage } from '../../pages/appointment-selection/appointment-selection';
import { SectionPerfilPage } from '../section-perfil/section-perfil';


@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {
img: any = "assets/imgs/log.png"
datos:any = [];
  constructor(private menu: MenuController,public alertCtrl: AlertController,public loadingCtrl: LoadingController,public restangular: Restangular,public modalCtrl : ModalController, public navCtrl: NavController, public navParams: NavParams) {
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.cargar().then( () => {
      // this.publicidad();
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }

  cargar(){
    return new Promise(resolve => {
      this.restangular.one('favoritosuser').customGET('',{}).toPromise()
        .then((data)=> {
          console.log(data.data);
                  
          this.datos = data.data;
          // console.log(this.dataavatars);          
          resolve(true);
        },
        ()=>{
          resolve(false);
        });
    });
  }


  openFavorite(page,item){
    if (page == 'c') {
      this.navCtrl.push(AppointmentSelectionPage,{person:item})
    }else{
      this.navCtrl.push(SectionPerfilPage,{data:item})

    }
	
  }
  presentProfileModal() {
    let profileModal = this.modalCtrl.create(PublicityPage, { userId: 8675309 });
    profileModal.present();
  }

  eliminar(data){
    let alert = this.alertCtrl.create({
      title: 'Confirmar',
      message: '¿Quieres quitar de tu lista de favoritos?',
      cssClass: "alertcl",
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {
            console.log('Buy clicked');
            let loading = this.loadingCtrl.create({
              spinner: 'circles',
              // content: 'Recuperando datos.'
            });
            loading.present();
            this.restangular.one('quitarfavorito',data.id).remove()
            .subscribe((response)=> {
              console.log(response);
              loading.dismiss();
              this.cargar();
            },
            ()=>{
              loading.dismiss();
            });
          }
        }
      ]
    });
    alert.present();

    // let loading = this.loadingCtrl.create({
    //   spinner: 'circles',
    //   // content: 'Recuperando datos.'
    // });
    // loading.present();
    // dialogRef.afterClosed().subscribe(result => {
    //   if(result){
    //     this.carga = true;
    //     data.remove().subscribe( (response) => {
    //       this.carga = false;
    //       // Updating the list and removing the user after the response is OK.
    //       this.cargar();
    //       this.snackBar.open(response.mensaje, ':-)', {
    //         duration: 3000,
    //       });
    //     },
    //     ()=>{
    //       this.carga = false;
    //     });
    //   }
    // });
  }

}
