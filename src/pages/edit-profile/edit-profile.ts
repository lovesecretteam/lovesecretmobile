import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  data = [];
  sexo = [{name:'Masculino', value:'Masculino'},{name:'Femenino',value:'Femenino'}]
contextura = [{name:'Pequeña', value:'Pequeña'},{name:'Mediana',value:'Mediana'},{name:'Grande',value:'Grande'}]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

}
