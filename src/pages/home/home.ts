import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,ModalController } from 'ionic-angular';
import { ServicesPage } from '../../pages/services/services';
import { Restangular } from 'ngx-restangular';
import { PublicityPage } from '../publicity/publicity';

import { from } from 'rxjs/observable/from';
import { SectionPage } from '../section/section';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  servicios: any = [];
  img: any = "assets/imgs/log.png";
  datos:any = [];
  publicidads:any = [];
  constructor(public modalCtrl : ModalController, private restangular:Restangular,public loadingCtrl: LoadingController,public navCtrl: NavController) {
    this.publicidads = JSON.parse( localStorage.getItem('publicidads') );
    console.log(this.publicidads);
    
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.cargar().then( () => {
      // this.publicidad();
      loading.dismiss();
    });
  }
  onInit(){
    
  }

  cargar(){
    return new Promise(resolve => {
      this.restangular.one('menu').customGET('',{}).toPromise()
        .then((data)=> {
          console.log(data.data);
                  
          this.datos = data.data;
          // console.log(this.dataavatars);
          
          resolve(true);
        },
        ()=>{
          resolve(false);
        });
    });
  }


  openHome(item){
    if(item.id == 1){
      console.log(item);
      this.navCtrl.push(ServicesPage,{item:item});
    }else{
      console.log("carga");
      
      this.navCtrl.push(SectionPage,{item:item});
    }
    
  }
  presentProfileModal(item) {
    console.log(item);
    
    let profileModal = this.modalCtrl.create(PublicityPage, { data: item });
    profileModal.present();
  }


}
