import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-city',
  templateUrl: 'city.html',
})
export class CityPage {
public items: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = navParams.data.item;
    console.log(this.items);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CityPage');
  }

}
