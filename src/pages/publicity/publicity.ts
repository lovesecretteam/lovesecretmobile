import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ViewController } from 'ionic-angular';

/**
 * Generated class for the PublicityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-publicity',
  templateUrl: 'publicity.html',
})
export class PublicityPage {
@ViewChild('slider') slider: Slides;
 concerts = [
    {
      name: 'yeezy world tour 2017',
      artistName: 'Kanye West',
      artistImage: 'assets/imgs/misc/kanye_west.png',
      color: '#f73e53'
    },
    {
      name: 'yeezy world tour 2017',
      artistName: 'Kanye West',
      artistImage: 'assets/imgs/misc/kanye_west.png',
      color: '#0be3ff'
    },
    {
      name: 'yeezy world tour 2017',
      artistName: 'Kanye West',
      artistImage: 'assets/imgs/misc/kanye_west.png',
      color: '#fdd427'
    },
  ];
  img: any = "assets/imgs/log.png"
  data:any;
  constructor(public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams) {
    this.data = this.navParams.get('data')
    console.log(this.data);
    
  }

  //  cardTapped(card) {
  //   alert(card.title + ' was tapped.');
  // }

  // share(card) {
  //   alert(card.title + ' was shared.');
  // }

  // listen(card) {
  //   alert('Listening to ' + card.title);
  // }

  // favorite(card) {
  //   alert(card.title + ' was favorited.');
  // }

  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

} 
