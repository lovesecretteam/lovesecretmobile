import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicityPage } from './publicity';

@NgModule({
  declarations: [
    PublicityPage,
  ],
  imports: [
    IonicPageModule.forChild(PublicityPage),
  ],
})
export class PublicityPageModule {}
