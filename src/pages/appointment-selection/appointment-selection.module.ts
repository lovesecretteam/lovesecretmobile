import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentSelectionPage } from './appointment-selection';

@NgModule({
  declarations: [
    AppointmentSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentSelectionPage),
  ],
})
export class AppointmentSelectionPageModule {}
