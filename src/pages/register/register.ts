import { Component } from '@angular/core';
import {
  IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController,
  AlertController
} from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { IUsuario } from "../../model/models";
import { Restangular } from "ngx-restangular";


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  dato:IUsuario = {id:0,alias:"",avatar_id:0,username:"",password:""};
  avatar:any;
  constructor(public alertCtrl: AlertController, private toastCtrl: ToastController,public restangular: Restangular,private auth: AuthProvider,public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController) {
    this.avatar = navParams.get('data');
    this.dato.avatar_id = this.avatar.id;
  
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  openHome(){
    this.navCtrl.setRoot(HomePage);
  }
  confirm(){
    
  }
  registrar(){
    const confirm = this.alertCtrl.create({
      title: 'Es mayor de edad?',
      message: 'Este SEX SHOP contiene material sólo para adultos, que puede ser ofensivo para algunas personas Pulse aceptar si es mayor de edad?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
 let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Registrando..'
    });
    loading.present();
    this.restangular.all('regisusu').post(this.dato).subscribe((response) => {
        const toast = this.toastCtrl.create({
          message: "Registrado Correctamente",
          duration: 3000,
          position: 'bottom'
        });
        toast.onDidDismiss(() => {
          // this.disableInput = false;
          loading.dismiss();
        });
        toast.present();
        this.navCtrl.setRoot(LoginPage);
      },
      (error) =>{
        // console.log(error);
        
        // this.disableInput = false;
        loading.dismiss();
      });
            }
          }
        ]
    });
    confirm.present();
  }
}
