import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,ModalController, MenuController } from 'ionic-angular';
import { CityPage } from '../../pages/city/city';
import { Restangular } from 'ngx-restangular';


@IonicPage()
@Component({
  selector: 'page-country',
  templateUrl: 'country.html',
})
export class CountryPage {

  img: any = "assets/imgs/log.png";
  datos:any = [];
  constructor(private menu: MenuController,public modalCtrl : ModalController, private restangular:Restangular,public loadingCtrl: LoadingController,public navCtrl: NavController) {
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.cargar().then( () => {
      // this.publicidad();
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountryPage');
  }
  openCountry(item){
    this.navCtrl.push(CityPage,{item:item})
  }

  cargar(){
    return new Promise(resolve => {
      this.restangular.one('paisciudads').customGET('',{}).toPromise()
        .then((data)=> {
          console.log(data.data);
                  
          this.datos = data.data;
          // console.log(this.dataavatars);
          
          resolve(true);
        },
        ()=>{
          resolve(false);
        });
    });
  }

}
