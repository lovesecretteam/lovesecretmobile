import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailFavoritePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-favorite',
  templateUrl: 'detail-favorite.html',
})
export class DetailFavoritePage {
img: any = "assets/imgs/log.png"
servicios: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.servicios = [
        {name:'Rosita',descripcion:'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta)',numero:'76499880', zona:'Zona Norte',ciudad:'Cochabamba', conexion:'Online', profile:'assets/imgs/fotos/rosita.jpg', imgCliente:[{img:'assets/imgs/fotos/trip_5.jpg'},{img:'assets/imgs/fotos/trip_5.jpg'}], ambiente: [{nameambiente:'propio'},{nameambiente:'solo salidas'}], servicios: [{nameServicio:'Despedidas'},{nameServicio:'Desvirgadas'}]},
        {name:'Pepa',descripcion:'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias',numero:'76499880', zona:'Zona Norte',ciudad:'Cochabamba', conexion:'Online',  profile:'assets/imgs/fotos/pepa.jpg', imgCliente:[{img:'assets/imgs/fotos/pepa.jpg'},{img:'assets/imgs/fotos/pepa.jpg'}], ambiente: [{nameambiente:'propio'},{nameambiente:'solo salidas'}], servicios: [{nameServicio:'Despedidas'},{nameServicio:'Desvirgadas'}]}
      ]
  }

  ionViewDidLoad() {
  	console.log(this.servicios.imgCliente);
    console.log('ionViewDidLoad DetailFavoritePage');
  }

}
