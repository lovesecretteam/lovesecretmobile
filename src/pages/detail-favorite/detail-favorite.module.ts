import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailFavoritePage } from './detail-favorite';

@NgModule({
  declarations: [
    DetailFavoritePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailFavoritePage),
  ],
})
export class DetailFavoritePageModule {}
