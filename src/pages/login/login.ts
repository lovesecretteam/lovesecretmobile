import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { HomePage } from '../../pages/home/home';
import { RegisterPage } from '../../pages/register/register';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthProvider } from '../../providers/auth/auth';
import { ILogin } from "../../model/models";
import { AvatarsPage } from '../avatars/avatars';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  backgrounds = [
    'assets/imgs/slides/11.png',
    'assets/imgs/slides/22.png',
    'assets/imgs/slides/33.png'
    // 'assets/imgs/background-4.jpg'
  ];
  public loginForm: any;
  dato:ILogin = {username:"",password:""};
  constructor(public loadingCtrl: LoadingController, public toastCtrl: ToastController, private aut: AuthProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  openHome(){
    this.navCtrl.setRoot(HomePage);

  }
  openRegister(){
    this.navCtrl.push(AvatarsPage);

  }

  login() {
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Verificando usuario.'
    });
    loading.present();
      this.aut.login(this.dato).subscribe(
          data => {
            loading.dismiss();
            // this.navCtrl.setRoot(HomePage, {}, {animate: true});
            const toast = this.toastCtrl.create({
      			    message: "Logueado Correctamente",
      			    duration: 3000,
      			    position: 'bottom'
      			});
      			toast.onDidDismiss(() => {
              console.log('Dismissed toast');
      		  });
            toast.present();
            this.navCtrl.setRoot(HomePage);
          },
          err => {
            // console.log(err);
            loading.dismiss();
            const toast = this.toastCtrl.create({
                message: "Error al Iniciar Sesión",
                duration: 3000,
                position: 'bottom'
            });
            toast.onDidDismiss(() => {
            });
            toast.present();
          }
      );
  }
          
  ionViewCanEnter() {
    if(this.aut.isAuthenticated(['cliente','usuario'])){
      this.navCtrl.setRoot(HomePage);
    }
  }
}
