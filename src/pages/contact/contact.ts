import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PublicityPage } from '../publicity/publicity';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
img: any = "assets/imgs/log.png"
coverImage= 'assets/imgs/portada.jpg'

  constructor(public modalCtrl : ModalController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }
  presentProfileModal() {
    let profileModal = this.modalCtrl.create(PublicityPage, { userId: 8675309 });
    profileModal.present();
  }

}
