import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, MenuController, LoadingController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PublicityPage } from '../publicity/publicity';
import { Restangular } from "ngx-restangular";

/**
 * Generated class for the SectionPerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-section-perfil',
  templateUrl: 'section-perfil.html',
})

export class SectionPerfilPage {

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};
data:any;
img: any = "assets/imgs/log.png";
publicidads:any = [];
constructor(public loadingCtrl: LoadingController,public restangular: Restangular, private menu: MenuController,public modalCtrl : ModalController, public alertCtrl: AlertController, private socialShare: SocialSharing, private callNumber: CallNumber, private iab: InAppBrowser, public navCtrl: NavController, public navParams: NavParams) {
    this.publicidads = JSON.parse( localStorage.getItem('publicidads') );
    this.data = this.navParams.get('data'); 
    console.log(this.data);
    
  }

  ionViewDidLoad() {
    
  }

  openCallNumber(data){
  this.callNumber.callNumber(data, true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
  }

  public openWithSystemBrowser(data){
    console.log(data);
    let target = "_system";
    this.iab.create('https://api.whatsapp.com/send?phone=591'+data+'&text=Hola%20te%20mando%20un%20mensaje%20desde%20tourSecret%20mandame%20tu%20Pack%20cariño%20<3.',target,this.options);
  }

  presentProfileModal(item) {
    console.log(item);
    
    let profileModal = this.modalCtrl.create(PublicityPage, { data: item });
    profileModal.present();
  }

  vermapa(){
    
  }
  addfavorito(){
    if(this.data.favoritos == 'no' ){
      let tabla = "";
      if(this.data.club){
        tabla = "clubs";
      }
      if(this.data.hospedaje){
        tabla = "hospedaje";
      }
      if(this.data.empresa){
        tabla = "radiotaxi"; 
      }
      let loading = this.loadingCtrl.create({
        // spinner: 'circles',
        content: 'Agregando favorito.'
      });
      loading.present();
      let data = {
        id: this.data.id,
        accion: "favoritos",
        tabla: tabla
      };
      this.restangular.all('addaccion').post(data).subscribe( (response) => {
        loading.dismiss();
        this.data.favoritos = "si";
        console.log(response);
      });
    }
  }
}
