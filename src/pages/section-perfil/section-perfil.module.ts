import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SectionPerfilPage } from './section-perfil';

@NgModule({
  declarations: [
    SectionPerfilPage,
  ],
  imports: [
    IonicPageModule.forChild(SectionPerfilPage),
  ],
})
export class SectionPerfilPageModule {}
