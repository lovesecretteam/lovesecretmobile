import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, MenuController  } from 'ionic-angular';
import { SelectionPage } from '../../pages/selection/selection';
import { SectionPage } from '../../pages/section/section';
import { Restangular } from 'ngx-restangular';
import { PublicityPage } from '../publicity/publicity';

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {
    img: any = "assets/imgs/log.png"

// public items: any;
public itemSelected: any;
datos: any = [];
publicidads:any = [];
  constructor(private menu: MenuController,public modalCtrl : ModalController, private restangular:Restangular,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    this.publicidads = JSON.parse( localStorage.getItem('publicidads') );
    // this.items = navParams.data.item.Type;
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.cargar().then( () => {
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
		// console.log(this.items);
  }

  cargar(){
    return new Promise(resolve => {
      this.restangular.one('servicios').customGET('',{}).toPromise()
        .then((data)=> {
          console.log(data.data);
          
          this.datos = data.data;
          // console.log(this.dataavatars);
          
          resolve(true);
        },
        ()=>{
          resolve(false);
        });
    });
  }
  openService(item){
  	// this.itemSelected = item.Clients
  	// console.log(this.itemSelected);
  	 this.navCtrl.push(SelectionPage,{item:item})
  }
  presentProfileModal(item) {
    console.log(item);
    
    let profileModal = this.modalCtrl.create(PublicityPage, { data: item });
    profileModal.present();
  }

}
