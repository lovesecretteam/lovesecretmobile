import { ViewChild, ElementRef, Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { ImagenClientePage } from '../../pages/imagen-cliente/imagen-cliente';
import { Restangular } from 'ngx-restangular';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  @ViewChild('myInput') myInput: ElementRef;
public show = true;
img: any = "assets/imgs/log.png"
user = {
  name: 'Oscar Lizarazu Montaño Premium',
  profileImage: 'assets/imgs/perfil.jpg',
  coverImage: 'assets/imgs/portada.jpg',
  occupation: 'Instructor',
  location: 'Cochabamba, Cercado',
  ci:'8035038',
  direccion:'Av. Petrolera',
  description: 'A wise man once said: The more you do something, the better you will become at it.',
  certificaciones:[{nombre:'Certificacion Spinning'},{nombre:'Certificacion Zumba'},{nombre:'Certificacion Kombat'},{nombre:'Certificacion Karate'}],
  estado: true
};

sexo = [{name:'Masculino', value:'Masculino'},{name:'Femenino',value:'Femenino'}]
contextura = [{name:'Pequeña', value:'Pequeña'},{name:'Mediana',value:'Mediana'},{name:'Grande',value:'Grande'}]
  data:any;
  constructor(private restangular:Restangular,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams) {
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.cargar().then( () => {
      // this.publicidad();
      loading.dismiss();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  resize() {
    this.myInput.nativeElement.style.height = this.myInput.nativeElement.scrollHeight + 'px';
  }
  editProfile(){
    this.navCtrl.push(EditProfilePage)
  }
  editImagen(){
    this.navCtrl.push(ImagenClientePage)
  }

  cargar(){
    return new Promise(resolve => {
      this.restangular.one('userdata').customGET('',{}).toPromise()
        .then((data)=> {
          console.log(data.data);
                  
          this.data = data.data;
          // console.log(this.dataavatars);
          
          resolve(true);
        },
        ()=>{
          resolve(false);
        });
    });
  }
  
}
