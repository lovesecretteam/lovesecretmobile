import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController,MenuController, AlertController } from 'ionic-angular';
import { AppointmentSelectionPage } from '../../pages/appointment-selection/appointment-selection';
import { Restangular } from 'ngx-restangular';
import { PublicityPage } from '../publicity/publicity';
import { SectionPerfilPage } from '../section-perfil/section-perfil';
import { InfiniteScroll } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-section',
  templateUrl: 'section.html',
})
export class SectionPage {
  img: any = "assets/imgs/log.png";
  public items: any;
  public itemSelected: any;
  estado;
  checked;
  datos: any = [];
  paginador:any={
    actual_page:0,
    ultimo_page:0,
    total:0
  };
  noRecords:boolean = false;
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;
  publicidads:any = [];
  constructor(public alertCtrl: AlertController,private menu: MenuController,public modalCtrl : ModalController, private restangular:Restangular,public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {
    this.publicidads = JSON.parse( localStorage.getItem('publicidads') );
    this.items = navParams.data.item;
    console.log(this.items);
    
    let loading = this.loadingCtrl.create({
      // spinner: 'circles',
      content: 'Recuperando datos.'
    });
    loading.present();
    this.cargar('').then( () => {
      console.log(this.datos);

      loading.dismiss();
    });
  }

  cargar(busca){
    // infiniteScroll.enable(true);
    this.noRecords = false;
    return new Promise(resolve => {
      // let dep = JSON.parse(localStorage.getItem('departamento'));
      let query = {
        menu_id: this.items.id,
        order: '1',//ASC DESC RAND
        columna: 'id',
        limit: 5,
        filter:busca,
        page: this.paginador.actual_page+1,
      };
      
      if(query.page==1) {
        this.datos = [];
      }
      this.restangular.all('sections').getList(query).toPromise()
      .then((data)=> {
          console.log(data);
          let pagination = data[0];
          data.shift();
          if(data.length > 0) {
            this.paginador.actual_page = pagination.current_page;
            this.paginador.ultimo_page = pagination.last_page;
            this.paginador.total = pagination.total;
            for(let cli of data) {
              this.datos.push(cli);
            }
          }
          resolve(true);
      });
    });
  }

  itemSelection(item) {
    console.log('dsdsdsdsdsdsdsds');
	//  let nombre = this.items[item].name;
  //   let descripcion = this.items[item].descripcion;
  //   let imgCliente = this.items[item].imgCliente;
  //   let servicios =  this.items[item].servicios;
  //   let ambiente =  this.items[item].ambiente;
  //   let conexion =  this.items[item].conexion;
  //   let numero =  this.items[item].numero;
  //   let zona =  this.items[item].zona;
  //   let estado = this.items[item].estado;
    // console.log(numero);
  	this.navCtrl.push(SectionPerfilPage,{data:item})
  }
  buscar(){
    const prompt = this.alertCtrl.create({
      title: '¿Que buscas?',
      message: "Morenas, chocas, chinas algun servicio o nombre especifico",
      inputs: [
        {
          name: 'busca',
          placeholder: 'Busco'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log(data);
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Buscar',
          handler: data => {
            console.log(data.busca);
            let loading = this.loadingCtrl.create({
              // spinner: 'circles',
              content: 'Recuperando datos.'
            });
            loading.present();
            this.paginador.actual_page = 0;
            this.cargar(data.busca).then( () => {
              console.log(this.datos);
        
              loading.dismiss();
            });
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }
  presentProfileModal(item) {
    console.log(item);
    
    let profileModal = this.modalCtrl.create(PublicityPage, { data: item });
    profileModal.present();
  }

  loadData(event) {
    console.log(event);
    this.cargar("").then( () => {
      event.complete();
      if(this.paginador.actual_page === this.paginador.ultimo_page){
        console.log("completa la carga");
        // event.enable(false);
        this.noRecords = true;
      }
    });
    // setTimeout(() => {
    //   console.log('Done cargando infiniti');
    //   event.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      // if (data.length == 1000) {
      //   event.target.disabled = true;
      // }
    // }, 500);
  }
}
