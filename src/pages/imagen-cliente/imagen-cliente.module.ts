import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImagenClientePage } from './imagen-cliente';

@NgModule({
  declarations: [
    ImagenClientePage,
  ],
  imports: [
    IonicPageModule.forChild(ImagenClientePage),
  ],
})
export class ImagenClientePageModule {}
