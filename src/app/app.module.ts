import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ServicesPage } from '../pages/services/services';
import { SelectionPage } from '../pages/selection/selection';
import { AppointmentSelectionPage } from '../pages/appointment-selection/appointment-selection';
import { PublicityPage } from '../pages/publicity/publicity';
import { ListPublicityPage } from '../pages/list-publicity/list-publicity';
import { PoliticsPage } from '../pages/politics/politics';
import { FavoritesPage } from '../pages/favorites/favorites';
import { ProfilePage } from '../pages/profile/profile';
import { ContactPage } from '../pages/contact/contact';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { DetailFavoritePage } from '../pages/detail-favorite/detail-favorite';
import { AvatarsPage } from '../pages/avatars/avatars';
import { CountryPage } from '../pages/country/country';
import { CityPage } from '../pages/city/city';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { SectionPage } from '../pages/section/section';
import { SectionPerfilPage } from '../pages/section-perfil/section-perfil';
import { ImagenClientePage } from '../pages/imagen-cliente/imagen-cliente';

import { HttpClientModule } from '@angular/common/http';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';
import { TooltipsModule } from 'ionic-tooltips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthProvider } from '../providers/auth/auth';
import { RestangularModule } from 'ngx-restangular';
import { RestangularConfigFactory } from '../providers/restangular/restangular';
import { TokenStorageProvider } from '../providers/token-storage/token-storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ServicesPage,
    SelectionPage,
    AppointmentSelectionPage,
    PublicityPage,
    ListPublicityPage,
    PoliticsPage,
    FavoritesPage,
    ProfilePage,
    ContactPage,
    LoginPage,
    RegisterPage,
    DetailFavoritePage,
    AvatarsPage,
    CountryPage,
    CityPage,
    EditProfilePage,
    SectionPage,
    SectionPerfilPage,
    ImagenClientePage
  ],
  imports: [
    IonicImageViewerModule,
    TooltipsModule,
    BrowserModule,
    BrowserAnimationsModule,
    RestangularModule.forRoot([AuthProvider],RestangularConfigFactory),
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ServicesPage,
    SelectionPage,
    AppointmentSelectionPage,
    PublicityPage,
    ListPublicityPage,
    PoliticsPage,
    FavoritesPage,
    ProfilePage,
    ContactPage,
    LoginPage,
    RegisterPage,
    DetailFavoritePage,
    AvatarsPage,
    CountryPage,
    CityPage,
    EditProfilePage,
    SectionPage,
    SectionPerfilPage,
    ImagenClientePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    CallNumber,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    TokenStorageProvider,
    SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
