import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ContactPage } from '../pages/contact/contact';
import { ProfilePage } from '../pages/profile/profile';
import { FavoritesPage } from '../pages/favorites/favorites';
import { PublicityPage } from '../pages/publicity/publicity';
import { ListPublicityPage } from '../pages/list-publicity/list-publicity';

import { PoliticsPage } from '../pages/politics/politics';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { CountryPage } from '../pages/country/country';
import { AuthProvider } from '../providers/auth/auth';
import { Restangular } from 'ngx-restangular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  // rootPage: any = HomePage;
  img: any = "assets/imgs/log.png"

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(private restangular:Restangular,private aut: AuthProvider,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    this.publicidad();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage, icon: 'ios-home' },
      { title: 'Mis Favoritos', component: FavoritesPage, icon: 'ios-star-half' },
      { title: 'Mi Cuenta', component: ProfilePage, icon: 'ios-contact' },
      { title: 'Publicidad', component: ListPublicityPage, icon: 'ios-megaphone'},
      { title: 'Contáctanos', component: ContactPage, icon: 'ios-call' },
      { title: 'Politicas', component: PoliticsPage, icon: 'ios-list-box' },
      { title: 'Ciudad', component: CountryPage, icon: 'ios-navigate' }

    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    
    // if(page.title == 'Mis Favoritos' || page.title ==  'Publicidad' || page.title ==  'Publicidad'){
      this.nav.push(page.component);
    // }else{
      // this.nav.setRoot(page.component);
    // }
  }
  logout(){
    console.log("cerrar secion");
    
    this.aut.logout().subscribe(
      data => {
        console.log("terminado");
        
      }
    );
  }

  publicidad(){ 
    this.restangular.one('publicidads').customGET('',{}).toPromise()
    .then((data)=> {
      console.log(data.data);
      localStorage.setItem('publicidads', JSON.stringify(data.data));//mm

      // this.publicidads = data.data;
      // console.log(this.publicidads);
    },
    ()=>{
      console.log("error");
      
    });
  }
}
